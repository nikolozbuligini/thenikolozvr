import React from 'react';
import {
  Cylinder,
  Sphere,
  SpotLight,
  Animated,
  asset,
  StyleSheet,
  Pano,
  Text,
  View,
  Model,
} from 'react-vr';
import WorkExperience from './components/WorkExperience';

class App extends React.Component {
  state = {
    rotate: 0,
    bounceValue: new Animated.Value(0),
    active: '',
  }

  componentDidMount() {
    // setInterval(() => {
    //   const { rotate } = this.state;
    //   this.setState({ rotate: rotate + 2 });
    // }, 50);

    this.state.bounceValue.setValue(1.2);     // Start large
    Animated.spring(                          // Base: spring, decay, timing
      this.state.bounceValue,                 // Animate `bounceValue`
      {
        toValue: 0.8,                         // Animate to smaller size
        friction: 1,                          // Bouncier spring
      }
    ).start();
  }

  onActivation = (button) => {
    console.log(button);
    this.setState({ active: button });
  }

  render() {
    return (
      <View>
        <Pano source={asset('office.jpg')}/>
        <Model
          source={{ obj: asset('Logo_V1.obj') }}
          style={{
            color: 'black',
            transform: [
              {translate: [-2, 0, -2]},
              { scale: 0.01 }
            ],
          }}
        />
        {/* <WorkExperience
          isActive={this.state.active === 'WorkExperience'}
          onClick={() => this.onActivation('WorkExperience')}
        /> */}
        <View>
          <Text
            style={{
              backgroundColor: '#777879',
              fontSize: 0.8,
              fontWeight: '400',
              layoutOrigin: [0.5, 0.5],
              paddingLeft: 0.2,
              paddingRight: 0.2,
              textAlign: 'center',
              textAlignVertical: 'center',
              transform: [
                {translate: [0, 0, -6]},
                {rotateY: this.state.rotate},
              ],
            }}>
            hello world
          </Text>
          <Animated.Image                         // Base: Image, Text, View
            source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}}
            style={{
              flex: 1,
              width: 1,
              height: 1,
              transform: [                        // `transform` is an ordered array
                {translate: [0, 2, -2]},
                {scale: this.state.bounceValue},  // Map `bounceValue` to `scale`
              ]
            }}
          />
        </View>
        <Cylinder
          radiusTop={0.5}
          radiusBottom={0.5}
          dimHeight={1}
          segments={24}
          texture={asset('texture.jpg')}
          style={{
            color: '#3F51B5',
          }}
        />
        <Sphere
          radius={0.5}
          widthSegments={24}
          heightSegments={12}
          texture={asset('globe.jpg')}
          style={{
            transform: [
              {translate: [0, 0, -1.2]},
              {rotateY: this.state.rotate},
            ],
          }}
        />
      </View>
    );
  }
}

export default App;
