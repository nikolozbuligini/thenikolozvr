import React from 'react';
import {
  AppRegistry,
  Animated,
  asset,
  StyleSheet,
  Text,
  View,
  VrButton,
} from 'react-vr';

class WorkExperience extends React.Component {
  state = {
    hasFocus: false,
  }

  render() {
    return (
      <View style={styles.container}>
        <VrButton
          onClick={this.props.onClick}
          onClickSound={{
             mp3: asset('click.mp3'),
            }}
          >
          <Text
            style={[styles.text, this.state.hasFocus && styles.activeButton]}
            onEnter={() => this.setState({ hasFocus: true })}
            onExit={() => this.setState({ hasFocus: false })}
          >
            WorkExperience
          </Text>
        </VrButton>
        {this.props.isActive && (
          <Text>
            Active
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    transform: [
      {translate: [0, 1, -3]},
    ],
  },
  text: {
    padding: 0.2,
    borderRadius: 0.1,
    backgroundColor: '#fff',
    color: '#999',
    fontWeight: '400',
    fontSize: 0.4,
    // layoutOrigin: [0.5, 0.5],
    textAlign: 'center',
    textAlignVertical: 'center',
    transform: [
      {translate: [0, 0, -3]},
    ],
  },
  activeButton: {
    backgroundColor: '#ddd',
  },
  title: {
    fontSize: 19,
    fontWeight: 'bold',
  },
  activeTitle: {
    color: 'red',
  },
});

export default WorkExperience;
